# efk_logging

## 說明
docker 方式
執行單機elasticsearch + kibana + nginx代理
fluentbit則可執行於遠程機器或本地 
類似agent(遠程push log回此efk系統)

各種log用parser拆解完，送回elasticsearch

效能需求低，比fluentd更輕量化

## 參考
- [ ] [fluent-bit introduction](https://docs.fluentbit.io/manual)

## nginx log format 格式 (main)
```
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for" '
                      '"$host" "$upstream_addr" "$upstream_status" '
                      '"$request_time" "$upstream_response_time" "$request_body"';
```
## nginx log format 格式 (default)
```
log_format  default  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
```

## apache log format 格式 (combined)
```
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
```

## 
依據你的格式去修改parser