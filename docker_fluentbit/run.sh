#!/bin/bash
set -e
TAGNAME=$1
TYPE=$2
LOG_ROOT_PATH=$3
ACCESSLOG_PATH=$4
ERRORLOG_PATH=$5
OUTPUT_SETTING=$6
# TAGNAME=custom name etc projectA/c01/customer01/etc.
# TYPE=Backend/Proxy/Frontend/etc..
# TAG will be c01-backend or projectA-proxy ..etc
TAG=${TAGNAME}-${TYPE}
MACHINE_IP=$(curl -s ifconfig.me)
echo "${TAG}"
echo "========"
echo "${MACHINE_IP}"

docker run \
--restart=always \
-d \
--name ${TAG} \
-w /app \
-v $PWD:/app \
-v $LOG_ROOT_PATH:$LOG_ROOT_PATH \
-v /usr/share/zoneinfo/Asia/Taipei:/etc/localtime:ro \
-e projectname="${TAG}" \
-e username="xxxxxx" \
-e password="xxxxxx" \
-e tag="${MACHINE_IP}" \
-e access_path="${ACCESSLOG_PATH}" \
-e error_path="${ERRORLOG_PATH}" \
fluent/fluent-bit:1.1 \
/fluent-bit/bin/fluent-bit -c "${OUTPUT_SETTING}"

## username and password your need to check what you set in htpasswd 
## (elasticsearch login user and password)