## fluent bit 打 log 到 elasticsearch

## 執行指令 nginx 範例
```sh run.sh myes proxy /home/data/docker_nginx/logs /home/data/docker_nginx/logs/es_access.log /home/data/docker_nginx/logs/es_error.log ./nginx_default.conf```
## 執行指令 apache 範例
```sh run.sh a004 backend /xxx/xxx/httpd-2.4.10/logs/access_log ./apache_default.conf```

## 參數說明
> 1. 專案名
> 2. 類型(前端/後端/代理/自定義等等)
> 3. 掛載日誌路徑
> 4. access log 路徑
> 5. error log 路徑
> 6. fluent-bit設定 (不同Input Parser/不同Output Host) (根據不同需求)

## 備註 
 - container name會由 $1 $2組成, es的index也會是這個container-name
 - elasticsearch裡面設定tag為發送log機器的ip，可依據需求修改(-e tag="${MACHINE_IP}")
